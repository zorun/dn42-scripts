#!/bin/sh

now=$(date +%s)

birdc 'show route' | grep "^[0-9]" | cut -d ' ' -f 1 | while read prefix
do
    echo "$prefix" "$now"
done > /tmp/bgp_dn42_lastseen

