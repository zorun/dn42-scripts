#!/usr/bin/env python

import sys
import time

import bird
from utils import read_json, write_json

DBFILE = '/srv/http/dn42/tower-bird.json'


def update(dbfile, stream):
    new = bird.parse_bird(stream)
    db = read_json(dbfile)
    bird.update(db, new)
    write_json(db, dbfile)

if __name__ == '__main__':
    update(DBFILE, sys.stdin)
