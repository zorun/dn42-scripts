#!/usr/bin/env python3

from collections import defaultdict
from pprint import pprint
import sys

from netaddr import IPSet, IPNetwork
from registry import Inetnum
from utils import read_json

class Count():
    # timestamp -> int (deletion or addition)
    count = defaultdict(int)

    def feed(self, line):
        data = line.split()
        timestamp = int(data[0])
        if data[1] == "add_file":
            self.count[timestamp] += 1
        elif data[1] == "delete":
            self.count[timestamp] -= 1

    def dump(self):
        return self.count

    def history(self):
        count = 0
        for timestamp in sorted(self.count):
            count += self.count[timestamp]
            print("{} {}".format(timestamp, count))

class Subnets():
    # timestamp -> (added subnets, removed subnets)
    space = defaultdict(lambda: (IPSet([]), IPSet([])))
    dn42 = IPSet(["172.22.0.0/15"])
    registry = Inetnum("/home/zorun/net.dn42.registry")

    def feed(self, timestamp, type, subnet):
        if not subnet in self.dn42:
            return
        if subnet in self.registry.data and not self.registry.data[subnet]["status"][0].lower().startswith("assigned"):
            return
        if data[1] == "add_file":
            self.space[timestamp][0].add(subnet)
        elif data[1] == "delete":
            self.space[timestamp][1].add(subnet)

    def history(self):
        current = IPSet(read_json("/srv/http/dn42/tower-bird.json"))
        used = IPSet([])
        for timestamp in sorted(self.space):
            used = used.difference(self.space[timestamp][1])
            used = used.union(self.space[timestamp][0])
            announced = used.intersection(current)
            print("{} {} {}".format(timestamp,
                                    float(used.size) / float(self.dn42.size),
                                    float(announced.size) / float(self.dn42.size)))
        return used
        

if __name__ == '__main__':
    persons = Count()
    inetnums = Subnets()
    for line in sys.stdin:
        data = line.split()
        if data[2].startswith('"data/person/'):
            persons.feed(line)
        elif data[2].startswith('"data/inetnum/'):
            subnet = data[2][14:-1].replace('_', '/')
            inetnums.feed(int(data[0]), data[1], subnet)

    #persons.history()
    result = inetnums.history()
    registry = (subnet for subnet in inetnums.registry.data.keys() if inetnums.registry.data[subnet]["status"][0].lower().startswith("assigned"))
    registry = IPSet(registry)
    
    pprint(result)
    print("\n\n")
    pprint(registry.intersection(inetnums.dn42).difference(result))
