# coding: utf-8

import netaddr
import datetime
import lglass.bird
import lglass.route
import lglass.database.file

with open("routes.bird") as fh:
	routes = lglass.route.RoutingTable(lglass.bird.parse_routes(fh))

db = lglass.database.file.FileDatabase("/home/zorun/net.dn42.registry/data")

dn42_native = netaddr.IPNetwork("172.22.0.0/15")
db_nets = netaddr.IPSet()
route_nets = netaddr.IPSet()

for type, primary_key in db.list():
	if type == "inetnum":
		addr = netaddr.IPNetwork(primary_key)
		if addr not in dn42_native:
			continue
		obj = db.get(type, primary_key)
		if "BLK" in obj.getfirst("netname", ""):
			continue
		db_nets.add(addr)

for route in routes:
	if route.prefix not in dn42_native:
		continue
	route_nets.add(route.prefix)

all_nets = netaddr.IPSet()
all_nets.add(dn42_native)
announced_nets = route_nets
registered_nets = db_nets
unused_nets = all_nets - announced_nets
unregistered_nets = all_nets - registered_nets
free_nets = unused_nets.intersection(unregistered_nets)

print("DN42 network usage report")
print("Date: {}".format(datetime.datetime.now()))
print()

print("Statistics:")
print("  Total        {}".format(len(all_nets)))
print("-" * 80)
print("  Announced    {}\t{}%".format(len(announced_nets), len(announced_nets)/len(all_nets)*100))
print("  Registered   {}\t{}%".format(len(registered_nets), len(registered_nets)/len(all_nets)*100))
print("  Unregistered {}\t{}%".format(len(unregistered_nets), len(unregistered_nets)/len(all_nets)*100))
print("  Unused       {}\t{}%".format(len(unused_nets), len(unused_nets)/len(all_nets)*100))
print("  Free         {}\t{}%".format(len(free_nets), len(free_nets)/len(all_nets)*100))
print()

print("Announced netwoks:")
for net in announced_nets.iter_cidrs():
	print(" * {}".format(net))
print()

print("Registered netwoks:")
for net in registered_nets.iter_cidrs():
	print(" * {}".format(net))
print()

print("Unregistered netwoks:")
for net in unregistered_nets.iter_cidrs():
	print(" * {}".format(net))
print()

print("Unused netwoks:")
for net in unused_nets.iter_cidrs():
	print(" * {}".format(net))
print()

print("Free netwoks:")
for net in free_nets.iter_cidrs():
	print(" * {}".format(net))
