import json

def read_json(filename):
    try:
        with open(filename, "r") as f:
            data = json.load(f)
    except FileNotFoundError as e:
        print("Warning:", e)
        print("Could not find json file {}, returning an empty dict".format(filename))
        data = dict()
    except ValueError as e:
        print("Warning:", e)
        print("Could not decode json data of file {}, returning an empty dict".format(filename))
        data = dict()
    return data

def write_json(data, filename):
    with open(filename, "w") as f:
        json.dump(data, f)
