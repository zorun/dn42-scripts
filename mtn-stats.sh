#!/bin/bash

[ -n "$1" ] && repo="." || repo="$1"

cd "$repo"

mtn automate log --no-merges | while read revision
do
    date="$(mtn automate certs "$revision" \
        | grep -A1 'name "date"' | tail -n 1 \
        | sed -e 's/.*\"\(.*\)\"/\1/')"
    timestamp="$(date --date="$date" +%s)"
    mtn automate get_revision "$revision" \
        | grep '^add_file\|^delete\|^rename\|^patch' \
        | sed -e "s/^/$timestamp /"
done
