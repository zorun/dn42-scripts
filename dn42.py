import os
from netaddr import IPNetwork, IPSet

# WARNING: deprecated lib, see 'registry.py' instead.

ROOT_PREFIX = "172.22.0.0/15"
ADDRSPACE = IPSet([ROOT_PREFIX])

class Prefix(object):
    prefix = None
    display = "none"

    def __init__(self, prefix):
        self.prefix = IPNetwork(prefix)

    def __str__(self):
        return str(self.prefix)

    def to_dict(self):
        d = dict()
        d["name"] = d["prefix"] = str(self.prefix)
        d["size"] = self.prefix.size
        if self.display:
            d["display"] = self.display
        return d


class UsedPrefix(Prefix):
    properties = dict()
    reserved_properties = {"name", "prefix", "size", "display"}

    def __init__(self, prefix, properties):
        self.properties = properties
        self.check_properties()
        super(UsedPrefix, self).__init__(prefix)
        self.display = None

    def check_properties(self):
        if self.reserved_properties.intersection(self.properties.keys()):
            raise ValueError

    def to_dict(self):
        d = super(UsedPrefix, self).to_dict()
        d.update(self.properties)
        return d


def parse_record(stream):
    """General parsing of the "key: value" syntax. Returns a key → [values]
    dictionary.
    """
    d = dict()
    for entry in stream.readlines():
        try:
            key, value = [s.strip() for s in entry.split(':', 1)]
            if not key in d:
                d[key] = list()
            d[key].append(value)
        except ValueError: pass
    return d


def parse_records(records_dir, fix_underscore=True):
    """Takes a directory containing records, and builds a dictionary mapping the
    filename of each record to its parsed data. By default, we transform '_'
    to '/' in the name of the records.
    """
    records = dict()
    for record in os.listdir(records_dir):
        record_path = os.path.join(records_dir, record)
        record_key = record.replace('_', '/') if fix_underscore else record
        with open(record_path, "r") as f:
            records[record_key] = parse_record(f)
    return records
