#!/usr/bin/env python

import os

from registry import Registry

OUTDIR = '/srv/http/dn42/registry'

if __name__ == '__main__':
    dn42 = Registry("/home/zorun/net.dn42.registry")
    for attr in ["dns", "inetnum", "inet6num", "route", "route6", "person",
                 "organisation", "mntner", "asblock", "asset", "autnum"]:
        d = dn42.__getattribute__(attr)
        outfile = os.path.join(OUTDIR, attr + '.json')
        with open(outfile, "w") as f:
            print("Writing {}...".format(outfile))
            d.write_json(f)
