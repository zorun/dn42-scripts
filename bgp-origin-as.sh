#!/bin/sh
#
# List all known BGP prefixes along with their origin AS. For Anycast
# prefixes, there may be multiple AS announcing the same prefixes, we
# display the one related to the best route we have.
#
# TODO: this script currently doesn't grab our own announced prefix.

MY_AS=76184

ssh -N -M -S '~/.ssh/master_%r-%h-%p' -o ControlPersist=10 mejis.polynome.dn42 >/dev/null &

sleep 1

prefixes="$(ssh mejis.polynome.dn42 -S '~/.ssh/master_%r-%h-%p' "vtysh -c 'show ip route'" | grep '^B>*' | cut -d ' ' -f 2)"

for prefix in $prefixes
do
    echo -n "$prefix "
    ssh mejis.polynome.dn42 -S '~/.ssh/master_%r-%h-%p' "vtysh -c 'show ip bgp $prefix'" \
        | grep -B 2 'best$' | head -1 \
        | sed -e 's/^ *//' -e 's/,.*//' \
        | awk '{ print $(NF); }' \
        | sed -e "s/Local/$MY_AS/"
done
