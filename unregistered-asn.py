#!/usr/bin/env python3

from registry import AutNum, Inetnum, Route
from utils import read_json

d = read_json("/srv/http/dn42/tower-bird.json")
autnum = AutNum("/home/zorun/net.dn42.registry")
inetnum = Inetnum("/home/zorun/net.dn42.registry")
route = Route("/home/zorun/net.dn42.registry")
unregistered_asn = set()
for prefix, data in d.items():
    if data["origin_as"] == "?":
        continue
    asn = "AS" + str(data["origin_as"])
    if not asn in autnum.data:
        unregistered_asn.add(data["origin_as"])

for asn in sorted(unregistered_asn):
    print("***********")
    print("* AS{} *".format(asn))
    print("***********\n")
    for prefix in sorted((pref for pref in d if d[pref]["origin_as"] == asn)):
        inum = inetnum.data[prefix]["netname"][0] if prefix in inetnum.data else ""
        rout = route.data[prefix]["descr"][0] if prefix in route.data else ""
        print("{} | {} | {}".format(prefix, inum, rout))
    print("\n")
