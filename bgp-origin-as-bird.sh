#!/bin/sh
#
# List all known BGP prefixes along with their origin AS. For Anycast
# prefixes, there may be multiple AS announcing the same prefixes.

ssh root@tower.polyno.me "birdc 'show route'" | grep "^[0-9]" | awk '{ print $1, substr($NF, 4, length($NF) - 5); }'
